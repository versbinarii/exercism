#[derive(Debug, PartialEq)]
pub struct DNA {
  chain: String,
}

#[derive(Debug, PartialEq)]
pub struct RNA {
  chain: String,
}

const DNA_SYMBOLS: &str = "ACGT";
const RNA_SYMBOLS: &str = "ACGU";

impl DNA {
  pub fn new(dna: &str) -> Result<DNA, usize> {
    if is_xna_valid(DNA_SYMBOLS, dna) {
      Ok(DNA {
        chain: dna.to_owned(),
      })
    } else {
      Err(0)
    }
  }
  pub fn to_rna(self) -> RNA {
    let rna = self.chain.chars().map(dna_2_rna).collect();
    RNA { chain: rna }
  }
}

impl RNA {
  pub fn new(rna: &str) -> Result<RNA, usize> {
    if is_xna_valid(RNA_SYMBOLS, rna) {
      Ok(RNA {
        chain: rna.to_owned(),
      })
    } else {
      Err(0)
    }
  }
}

fn is_xna_valid(symbols: &str, acid: &str) -> bool {
  acid.chars().all(|l| symbols.chars().any(|s| s == l))
}

fn dna_2_rna(letter: char) -> char {
  match letter {
    'A' => 'U',
    'G' => 'C',
    'C' => 'G',
    'T' => 'A',
    _ => '_',
  }
}
