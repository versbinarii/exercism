pub fn verse(v: u8) -> String {

    match v {
        0 => format!("No more bottles of beer on the wall, no more bottles of beer.\n\
                      Go to the store and buy some more, 99 bottles of beer on the wall.\n"),
        1 => format!("1 bottle of beer on the wall, 1 bottle of beer.\n\
                      Take it down and pass it around, no more bottles of beer on the wall.\n"),
        2 => format!("2 bottles of beer on the wall, 2 bottles of beer.\n\
                      Take one down and pass it around, 1 bottle of beer on the wall.\n"),
        bottle @_ => format!("{0} bottles of beer on the wall, {0} bottles of beer.\n\
                              Take one down and pass it around, {1} bottles of beer on the wall.\n", bottle, bottle-1)
    }
}

pub fn sing(from: u8, to: u8) -> String {
    let mut song = String::new();
    for v in (to..from+1).rev() {
        song.push_str(&verse(v));
        song.push('\n');
    }
    song.pop();
    song
}
