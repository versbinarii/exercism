pub fn rotate(input: &str, key: i8) -> String {
  input
    .chars()
    .map(|c| {
      if c.is_alphabetic() {
        if c.is_uppercase() {
          (((c as u8 - 'A' as u8 + key as u8) % 26) + 'A' as u8) as char
        } else {
          (((c as u8 - 'a' as u8 + key as u8) % 26) + 'a' as u8) as char
        }
      } else {
        c
      }
    })
    .collect()
}
