pub fn spiral_matrix(size: u32) -> Vec<Vec<u32>> {
  let mut spiral = vec![vec![0; size as usize]; size as usize];

  let mut row = 0;
  let mut col = 0;
  let mut delta_row: i8 = 0;
  let mut delta_col: i8 = 1;

  if size == 0 {
    return vec![];
  }
  if size == 1 {
    return vec![vec![1]];
  }

  let mut iterator = 0;
  while spiral[row as usize][col as usize] == 0 {
    iterator += 1;
    spiral[row as usize][col as usize] = iterator;
    if col + delta_col == size as i8
      || row + delta_row == size as i8
      || col + delta_col == -1
      || spiral[(row + delta_row) as usize][(col + delta_col) as usize] != 0
    {
      let tmp = delta_row;
      delta_row = delta_col;
      delta_col = -tmp;
    }

    row += delta_row;
    col += delta_col;
  }

  spiral
}
