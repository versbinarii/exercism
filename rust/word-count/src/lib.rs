use std::collections::HashMap;

/// Count occurrences of words.
pub fn word_count(words: &str) -> HashMap<String, u32> {
  let mut hmap = HashMap::new();

  let words_array = words.split(" ");
  for w in words_array {
    let w: String = w.chars().filter(|c| c.is_alphanumeric()).collect();
    if w.len() > 0 {
      let counter = hmap.entry(w.to_owned().to_lowercase()).or_insert(0);
      *counter += 1;
    }
  }
  hmap
}
