
macro_rules! modulo {

    ($a:expr, $b:expr) => {
        $a - $b * ($a / $b)
    }
}

pub fn nth(n: i32) -> Result<i32, ()>{
    if n < 1 {
        return Err(())
    }

    let mut primes: Vec<i32> = Vec::new();
    primes.push(2);
    primes.push(3);
    primes.push(5);

    // Start from odd then increment by 2
    let mut counter: i32 = 7;
    loop {

        // C = A % B == C = A - B * (A / B)
        if (modulo!(counter, 2) != 0) && (modulo!(counter, 3) != 0) {
            let mut div = 4;

            // better than sqrt ?
            while div*div <= counter {
                if counter % div == 0 {
                    break;
                }
                div += 1;
            }
            if div*div > counter {
                primes.push(counter);
            }
        }
        
        counter += 2;
        
        if primes.len() >= n as usize {
            break;
        }
    }
    Ok(primes[(n-1) as usize])
}
