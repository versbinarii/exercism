extern crate nth_prime as np;

#[test]
fn test_first_prime() {
    assert_eq!(np::nth(1), Ok(2));
}

#[test]
fn test_second_prime() {
    assert_eq!(np::nth(2), Ok(3));
    assert_eq!(np::nth(3), Ok(5));
    assert_eq!(np::nth(4), Ok(7));
    assert_eq!(np::nth(5), Ok(11));
}

#[test]
fn test_sixth_prime() {
    assert_eq!(np::nth(6), Ok(13));
}

#[test]
fn test_big_prime() {
    
    assert_eq!(np::nth(99999), Ok(1299689));
    assert_eq!(np::nth(10001), Ok(104743));
    
    assert_eq!(np::nth(999999), Ok(15485857));
}

#[test]
fn test_zeroth_prime() {
    assert!(np::nth(0).is_err());
}
