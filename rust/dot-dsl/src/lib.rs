pub mod graph {

  use std::collections::HashMap;

  use self::graph_items::edge::Edge;
  use self::graph_items::node::Node;

  #[derive(Debug)]
  pub struct Graph {
    pub nodes: Vec<Node>,
    pub edges: Vec<Edge>,
    pub attrs: HashMap<String, String>,
  }

  impl Graph {
    pub fn new() -> Self {
      Graph {
        nodes: Vec::new(),
        edges: Vec::new(),
        attrs: HashMap::new(),
      }
    }

    pub fn with_nodes(mut self, nodes: &[Node]) -> Self {
      self.nodes = nodes.to_vec();
      self
    }
    pub fn with_edges(mut self, edges: &[Edge]) -> Self {
      self.edges = edges.to_vec();
      self
    }

    pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
      self.attrs = attrs
        .iter()
        .map(|attr| (attr.0.to_string(), attr.1.to_string()))
        .collect();
      self
    }

    pub fn get_node(&mut self, name: &str) -> Option<Node> {
      let i = self.nodes.iter().position(|n| n.name == name)?;
      Some(self.nodes.swap_remove(i))
    }
  }

  pub mod graph_items {

    pub mod edge {
      #[derive(Clone, Debug, PartialEq)]
      pub struct Edge {
        start: String,
        end: String,
        attrs: Vec<(String, String)>,
      }

      impl Edge {
        pub fn new(start: &str, end: &str) -> Self {
          Edge {
            start: start.to_owned(),
            end: end.to_owned(),
            attrs: Vec::new(),
          }
        }

        pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
          self.attrs = attrs
            .iter()
            .map(|attr| (attr.0.to_string(), attr.1.to_string()))
            .collect();
          self
        }
      }
    }

    pub mod node {
      #[derive(Clone, Debug, PartialEq)]
      pub struct Node {
        pub(crate) name: String,
        attrs: Vec<(String, String)>,
      }
      impl Node {
        pub fn new(name: &str) -> Self {
          Node {
            name: name.to_owned(),
            attrs: Vec::new(),
          }
        }

        pub fn with_attrs(mut self, attrs: &[(&str, &str)]) -> Self {
          self.attrs = attrs
            .iter()
            .map(|attr| (attr.0.to_string(), attr.1.to_string()))
            .collect();

          self
        }

        pub fn get_attr(&self, name: &str) -> Option<&str> {
          let i = self.attrs.iter().position(|(k, _)| k == name)?;

          Some(&self.attrs.get(i).unwrap().1)
        }
      }
    }
  }
}
