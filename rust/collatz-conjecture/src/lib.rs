// return Ok(x) where x is the number of steps required to reach 1

pub fn collatz(n: u64) -> Result<u64, &'static str> {

    if n == 0 {
        return Err("Has to be positive and greater than 0.");
    }
    
    let mut number = n;
    let mut steps = 0;
    while number != 1 {
        if is_odd(number) {
            number = 3*number + 1
        } else {
            number = number /2;
        }
        steps += 1;
    }
    Ok(steps)
}

fn is_odd(n: u64) -> bool {
    n & 0x01 == 1 
}
