use std::collections::{HashMap, HashSet};
use std::iter::FromIterator;

const BALANCED_BRACKETS: &[(char, char)] = &[('{', '}'), ('[', ']'), ('(', ')')];

pub fn brackets_are_balanced(string: &str) -> bool {
    match string.is_empty() {
        true => true,
        false => {
            let mut stack: Vec<char> = Vec::new();
            let brackets: HashMap<char, char> =
                HashMap::from_iter(BALANCED_BRACKETS.iter().cloned());
            let brackets_check: HashSet<char> = BALANCED_BRACKETS
                .iter()
                .flat_map(|&(a, b)| vec![a, b].into_iter())
                .collect();

            for ref c in string.chars().filter(|c| brackets_check.contains(c)) {
                if brackets.contains_key(c) {
                    stack.push(*c);
                    continue;
                }
                if let Some(ref b) = stack.pop() {
                    // Safe to unwrap here as 2 first IFs ensure only valid brackets get here
                    if brackets.get(b).unwrap() == c {
                        continue;
                    }
                }
                return false;
            }
            stack.is_empty()
        }
    }
}
