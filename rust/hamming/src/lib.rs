pub fn hamming_distance(strand1: &str, strand2: &str) -> Result<usize, ()> {
    if strand1.len() != strand2.len() {
        return Err(());
    }

    let diff = strand1.bytes().zip(strand2.bytes()).filter(|&(ch1, ch2)| ch1 != ch2);
    Ok(diff.count())
}
