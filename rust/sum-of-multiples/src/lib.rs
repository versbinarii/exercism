macro_rules! modulo {

    ($a:expr, $b:expr) => {
        $a - $b * ($a / $b)
    }
}

pub fn sum_of_multiples(limit: u32, factors: &[u32]) -> u32 {

    (1..limit).filter(|i| {
        factors.iter().any(|x| {
            modulo!(i, x) == 0
        })
    }).sum()
}
