/// Determines whether the supplied string is a valid ISBN number
pub fn is_valid_isbn(isbn: &str) -> bool {
    if isbn.len() < 10 || isbn.len() > 13 {
        return false
    }
    // Check for invalid X
    let check: Vec<_> = isbn.split('X').collect();
    if check.len() > 1 && check[1].len() > 0 {
        return false
    }
    
    let result: u32 = isbn.chars().filter(|c| c.is_digit(10) || *c == 'X')
        .scan(11, |multiplier, d| {
            let digit: u32 = if d == 'X' { 10 } else { d.to_digit(10).unwrap_or(0) };
            
            *multiplier -= 1;
            Some(*multiplier * digit)
        }).sum();

    result % 11 == 0
}
