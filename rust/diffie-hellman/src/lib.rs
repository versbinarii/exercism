extern crate rand;
extern crate ramp;

use ramp::Int;
use rand::{Rng, thread_rng};

pub fn private_key(p: u64) -> u64 {
    let mut rng = thread_rng();
    rng.gen_range(2, p) as u64
}

pub fn public_key(p: u64, g: u64, a: u64) -> u64 {
    let gg = Int::from(g).pow(a as usize);
    let pow = u64::from(&gg);
    pow % p
}

pub fn secret(p: u64, b_pub: u64, a: u64) -> u64 {
    let b = Int::from(b_pub).pow(a as usize);
    let pow = u64::from(&b);
    pow % p
}
