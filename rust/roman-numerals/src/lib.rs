use std::fmt::{Display, Formatter, Result};

const ROMANS: &[&str] = &[
  "M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I",
];
const DECIMALS: &[u32] = &[1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];

pub struct Roman(String);

impl Display for Roman {
  fn fmt(&self, f: &mut Formatter) -> Result {
    write!(f, "{}", self.0);
    Ok(())
  }
}

impl From<u32> for Roman {
  fn from(num: u32) -> Self {
    let mut num = num;
    let mut roman_number = String::new();
    let _ = DECIMALS
      .iter()
      .enumerate()
      .map(|(i, d)| {
        while num % d < num {
          roman_number.push_str(ROMANS[i as usize]);
          num -= d;
        }
      }).collect::<Vec<_>>();

    Roman(roman_number)
  }
}
