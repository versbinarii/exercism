#[derive(Debug, Eq, PartialEq)]
struct Point(usize, usize);

pub fn find_saddle_points(input: &[Vec<u64>]) -> Vec<(usize, usize)> {
  let size: usize = input.iter().map(|v| v.len()).sum();
  if size == 0 {
    return vec![];
  }

  let rows = max_in_row(input);
  let cols = min_in_column(input);
  rows
    .iter()
    .filter(|point| cols.contains(point))
    .map(|p| (p.0, p.1))
    .collect()
}

fn min_in_column(input: &[Vec<u64>]) -> Vec<Point> {
  (0..input[0].len())
    .map(|col_idx| {
      let min_value = (0..input.len()).map(|i| input[i][col_idx]).min().unwrap();
      (0..input.len())
        .map(|i| input[i][col_idx])
        .enumerate()
        .filter(|(_, val)| val == &min_value)
        .map(|(i, _)| Point(i, col_idx))
        .collect()
    }).flat_map(|v: Vec<Point>| v)
    .collect()
}

fn max_in_row(input: &[Vec<u64>]) -> Vec<Point> {
  (0..input.len())
    .map(|row_idx| {
      let max_value = input[row_idx].iter().max().unwrap();
      input[row_idx]
        .iter()
        .enumerate()
        .filter(|(_, val)| val == &max_value)
        .map(|(i, _)| Point(row_idx, i))
        .collect()
    }).flat_map(|v: Vec<Point>| v)
    .collect()
}
