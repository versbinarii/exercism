pub fn factors(n: u64) -> Vec<u64> {
  let mut primes: Vec<u64> = Vec::new();
  let mut current_prime = 2;
  let mut number = n;
  loop {
    if number % current_prime == 0 {
      number /= current_prime;
      primes.push(current_prime);
    } else {
      current_prime = find_next_prime(current_prime);
    }
    if number == 1 {
      break;
    }
  }

  primes
}

fn find_next_prime(n: u64) -> u64 {
  let mut number = n;

  if n <= 2 {
    return 3;
  }
  loop {
    number += 2;
    if is_prime(number) {
      return number;
    }
  }
}

fn is_prime(n: u64) -> bool {
  for i in (3..n).step_by(2) {
    let remainder = n / i;
    if remainder * i == n {
      return false;
    }
    if remainder < i {
      return true;
    }
  }
  true
}

#[cfg(test)]
mod test {
  use super::find_next_prime;
  use super::is_prime;

  #[test]
  fn check_if_correct_next_prime() {
    assert_eq!(find_next_prime(2), 3);
    assert_eq!(find_next_prime(3), 5);
    assert_eq!(find_next_prime(7), 11);
    assert_eq!(find_next_prime(894109), 894119);
  }

  #[test]
  fn check_if_correctly_checks_prime() {
    assert_eq!(is_prime(894109), true);
    assert_eq!(is_prime(894119), true);
    assert_eq!(is_prime(29), true);
    assert_eq!(is_prime(12), false);
    assert_eq!(is_prime(67878456), false);
  }
}
