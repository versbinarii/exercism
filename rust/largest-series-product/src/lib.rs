#[derive(Debug, PartialEq)]
pub enum Error {
  SpanTooLong,
  InvalidDigit(char),
}

pub fn lsp(string_digits: &str, span: usize) -> Result<u64, Error> {
  if string_digits.len() < span {
    return Err(Error::SpanTooLong);
  }
  if span == 0 {
    return Ok(1u64);
  }
  let digits_list: Result<Vec<_>, _> = string_digits
    .chars()
    .map(|c| {
      if let Some(d) = c.to_digit(10) {
        return Ok(d as u64);
      } else {
        return Err(c);
      }
    }).collect();

  match digits_list {
    Ok(digits) => {
      let mut max = 0;
      for set in digits.windows(span) {
        let product = set.iter().product();
        if product > max {
          max = product;
        }
      }
      Ok(max)
    }
    Err(character) => Err(Error::InvalidDigit(character)),
  }
}
