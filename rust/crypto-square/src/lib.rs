pub fn encrypt(input: &str) -> String {
    if input.is_empty() {
        return "".to_string();
    }

    let norm = normalize(input);
    let (r, c) = find_dimensions(norm.len());

    (0..c)
        .map(|col| {
            (0..r)
                .map(|row| norm.iter().nth(row * c + col).unwrap_or(&' '))
                .collect::<String>()
        })
        .collect::<Vec<_>>()
        .join(" ")
}

fn normalize(input: &str) -> Vec<char> {
    input
        .to_lowercase()
        .chars()
        .filter(|c| c.is_alphanumeric())
        .collect()
}

fn find_dimensions(length: usize) -> (usize, usize) {
    let c = (length as f32).sqrt().ceil() as usize;
    let r = if length > c * (c - 1) { c } else { c - 1 };

    (r, c)
}
