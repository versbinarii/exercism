use std::io::{Read, Result, Write};

pub struct ReadStats<R> {
  reader: R,
  reads: usize,
  bytes_through: usize,
}

impl<R: Read> ReadStats<R> {
  // _wrapped is ignored because R is not bounded on Debug or Display and therefore
  // can't be passed through format!(). For actual implementation you will likely
  // wish to remove the leading underscore so the variable is not ignored.
  pub fn new(wrapped: R) -> ReadStats<R> {
    Self {
      reader: wrapped,
      reads: 0,
      bytes_through: 0,
    }
  }

  pub fn get_ref(&self) -> &R {
    &self.reader
  }

  pub fn bytes_through(&self) -> usize {
    self.bytes_through
  }

  pub fn reads(&self) -> usize {
    self.reads
  }
}

impl<R: Read> Read for ReadStats<R> {
  fn read(&mut self, buf: &mut [u8]) -> Result<usize> {
    let bytes_read = self.reader.read(buf)?;
    self.reads += 1;
    self.bytes_through += bytes_read;

    Ok(bytes_read)
  }
}

pub struct WriteStats<W> {
  writer: W,
  writes: usize,
  bytes_through: usize,
}

impl<W: Write> WriteStats<W> {
  // _wrapped is ignored because W is not bounded on Debug or Display and therefore
  // can't be passed through format!(). For actual implementation you will likely
  // wish to remove the leading underscore so the variable is not ignored.
  pub fn new(wrapped: W) -> WriteStats<W> {
    Self {
      writer: wrapped,
      writes: 0,
      bytes_through: 0,
    }
  }

  pub fn get_ref(&self) -> &W {
    &self.writer
  }

  pub fn bytes_through(&self) -> usize {
    self.bytes_through
  }

  pub fn writes(&self) -> usize {
    self.writes
  }
}

impl<W: Write> Write for WriteStats<W> {
  fn write(&mut self, buf: &[u8]) -> Result<usize> {
    let bytes_written = self.writer.write(buf)?;
    self.bytes_through += bytes_written;
    self.writes += 1;

    Ok(bytes_written)
  }

  fn flush(&mut self) -> Result<()> {
    self.writer.flush()
  }
}
