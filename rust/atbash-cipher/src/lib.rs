/// "Encipher" with the Atbash cipher.
pub fn encode(plain: &str) -> String {
  let mut cipher: String = plain
    .to_lowercase()
    .chars()
    .filter(|c| c.is_alphanumeric() && c.is_ascii())
    .enumerate()
    .map(|(i, c)| {
      let mut cc = convert(c).to_string();
      if (i + 1) % 5 == 0 {
        cc.push(' ');
      }
      cc
    }).collect();

  if cipher.ends_with(" ") {
    cipher.pop();
  }
  cipher
}

/// "Decipher" with the Atbash cipher.
pub fn decode(cipher: &str) -> String {
  let plaintext: String = cipher
    .chars()
    .filter(|c| c.is_alphanumeric())
    .map(convert)
    .collect();

  plaintext
}

fn convert(letter: char) -> char {
  if letter.is_numeric() {
    letter
  } else {
    ('z' as u8 - (letter as u8) + 'a' as u8) as char
  }
}
