pub fn build_proverb(list: Vec<&str>) -> String {
  let mut proverb: Vec<String> = vec!["".to_owned()];

  for i in 0..list.len() {
    let first = list.get(i).unwrap();
    if let Some(second) = list.get(i + 1) {
      proverb.push(format!(
        "For want of a {} the {} was lost.\n",
        first, second
      ));
    } else {
      proverb.push(format!("And all for the want of a {}.", list[0]));
    }
  }

  proverb.join("")
}
