/// What should the type of _function be?
pub fn map<N, M, F>(input: Vec<N>, mut function: F) -> Vec<M>
where
  F: FnMut(N) -> M,
{
  let mut res = vec![];
  for el in input {
    res.push(function(el));
  }
  res
}
