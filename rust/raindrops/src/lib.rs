fn factorize(number: i32) -> Vec<i32> {
    let mut factors: Vec<i32> = Vec::new();
    let mut factor = 1;
    while factor <= number {
        if (number % factor) == 0 {
            factors.push(factor);
        }
        factor += 1; 
    }
    factors
}

pub fn raindrops(number: i32) -> String {
    let mut ret_string = String::new();
    for f in factorize(number) {
        match f {
            3 => ret_string.push_str("Pling"),
            5 => ret_string.push_str("Plang"),
            7 => ret_string.push_str("Plong"),
            _ => {}
        }
    }
    if ret_string.is_empty() {
        ret_string.push_str(&number.to_string())
    }
    
    ret_string
}
