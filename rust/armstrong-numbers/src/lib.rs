pub fn is_armstrong_number(num: u32) -> bool {
  let digits: Vec<_> = num
    .to_string()
    .chars()
    .map(|d| d.to_digit(10).unwrap())
    .collect();

  let exp = digits.len() as u32;

  let check: u32 = digits.iter().map(|d| u32::pow(*d, exp)).sum();

  check == num
}
