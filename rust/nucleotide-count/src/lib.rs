use std::collections::HashMap;

pub fn count(nucleotide: char, dna: &str) -> Result<usize, char> {
  let mut count = 0;

  if let Err(x) = validate(dna) {
    return Err(x);
  }

  match nucleotide {
    'A' | 'C' | 'G' | 'T' => {
      let _ = dna
        .chars()
        .map(|c| {
          if c == nucleotide {
            count += 1;
          }
        }).collect::<Vec<_>>();
      Ok(count)
    }
    _ => Err(nucleotide),
  }
}

pub fn nucleotide_counts(dna: &str) -> Result<HashMap<char, usize>, char> {
  let valid = "ACGT";
  let mut result: HashMap<char, usize> = HashMap::new();

  if let Err(x) = validate(dna) {
    return Err(x);
  }

  let _: Vec<_> = valid
    .chars()
    .map(|c| result.insert(c, count(c, dna).unwrap()))
    .collect();
  Ok(result)
}

fn validate(dna: &str) -> Result<usize, char> {
  // Filter out the bad ones
  let errors: Vec<_> = dna
    .chars()
    .filter(|c| *c != 'A' && *c != 'C' && *c != 'G' && *c != 'T')
    .collect();

  if errors.len() > 0 {
    Err(errors[0])
  } else {
    Ok(0)
  }
}
