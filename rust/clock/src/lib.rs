use std::fmt;

#[derive(Debug, Eq, PartialEq)]
pub struct Clock {
  hours: i32,
  minutes: i32,
}

impl Clock {
  pub fn new(hours: i32, minutes: i32) -> Self {
    let mut h = hours.modulo(24);

    // Calc minutes
    let negative_or_positive_minutes = {
      if minutes < 0 {
        (minutes / 60) - 1
      } else {
        minutes / 60
      }
    };

    h = (h + negative_or_positive_minutes).modulo(24);
    let m = minutes.modulo(60);

    Self {
      hours: h,
      minutes: m,
    }
  }

  pub fn add_minutes(self, minutes: i32) -> Self {
    Self::new(self.hours, self.minutes + minutes)
  }
}

impl fmt::Display for Clock {
  fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
    write!(fmt, "{:02}:{:02}", self.hours, self.minutes);
    Ok(())
  }
}

// Modulo operation implementation
// since % is a remainder operator
trait Modulo {
  fn modulo(&self, num: i32) -> i32;
}

impl Modulo for i32 {
  fn modulo(&self, num: i32) -> i32 {
    let mut m = self % num;
    if m < 0 {
      m = {
        if num < 0 {
          m - num
        } else {
          m + num
        }
      }
    }
    m
  }
}
