use std::collections::HashMap;

#[derive(Debug, Ord, PartialOrd, Eq, PartialEq)]
struct Score {
  played: u32,
  won: u32,
  draw: u32,
  lost: u32,
  points: u32,
}

use std::default::Default;
impl Default for Score {
  fn default() -> Self {
    Self {
      played: 0,
      won: 0,
      draw: 0,
      lost: 0,
      points: 0,
    }
  }
}

impl Score {
  fn new() -> Self {
    Default::default()
  }

  fn update_score(&mut self, result: &str) {
    match result {
      "win" => {
        self.played += 1;
        self.won += 1;
        self.points += 3;
      }
      "loss" => {
        self.played += 1;
        self.lost += 1;
      }
      "draw" => {
        self.played += 1;
        self.draw += 1;
        self.points += 1;
      }
      _ => {}
    }
  }
}

use std::fmt;
impl fmt::Display for Score {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(
      f,
      "|  {} |  {} |  {} |  {} |  {}",
      self.played, self.won, self.draw, self.lost, self.points
    )
  }
}

pub fn tally(match_results: &str) -> String {
  let mut score: HashMap<String, Score> = HashMap::new();
  let mut result_table = "Team                           | MP |  W |  D |  L |  P".to_string();
  if match_results.len() == 0 {
    return result_table;
  }
  for line in match_results.split('\n') {
    let results = line.split(";").collect::<Vec<_>>();
    score
      .entry(results[0].to_owned())
      .or_insert(Score::new())
      .update_score(results[2]);
    score
      .entry(results[1].to_owned())
      .or_insert(Score::new())
      .update_score(get_oposite_result(results[2]));
  }

  let mut score: Vec<_> = score.drain().collect();
  score.sort_by(|(ka, va), (kb, vb)| vb.points.cmp(&va.points).then(ka.cmp(kb)));

  let _ = score
    .iter()
    .map(|(k, s)| result_table.push_str(&format!("\n{1:<0$}{2:}", 31, k, s)))
    .collect::<Vec<_>>();

  result_table
}

fn get_oposite_result(match_results: &str) -> &str {
  match match_results {
    "win" => "loss",
    "loss" => "win",
    "draw" => "draw",
    _ => "",
  }
}
