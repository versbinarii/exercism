pub struct PascalsTriangle {
    count: u32
}

impl PascalsTriangle {
    pub fn new(row_count: u32) -> Self {
        PascalsTriangle { count: row_count}
    }

    pub fn rows(&self) -> Vec<Vec<u32>>{
        let mut triangle: Vec<Vec<u32>> = Vec::new();
        for r in 0..self.count {
            let mut row = Vec::new();
            for c in 0..r+1 {
                row.push(pascal_value(r, c));
            }
            triangle.push(row);
        }
        triangle
    }
}

fn pascal_value(n: u32, r: u32) -> u32 {
    factorial(n) / (factorial(r) * factorial(n-r))
}

fn factorial(n: u32) -> u32 {
    let mut res = 1;
    for i in 2..n+1 {
        res *= i;
    }
    res
}

