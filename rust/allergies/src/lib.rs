pub struct Allergies(u32);

#[derive(Debug, PartialEq)]
pub enum Allergen {
  Eggs,
  Peanuts,
  Shellfish,
  Strawberries,
  Tomatoes,
  Chocolate,
  Pollen,
  Cats,
}

impl Allergies {
  pub fn new(score: u32) -> Self {
    Allergies(score)
  }

  pub fn is_allergic_to(&self, allergen: &Allergen) -> bool {
    let value = allergen2value(allergen);
    value & self.0 == value
  }

  pub fn allergies(&self) -> Vec<Allergen> {
    let value = 1;
    let mut allergens = vec![];
    for i in 0..8 {
      if let Some(alergen) = value2allergen(self.0 & (value << i)) {
        allergens.push(alergen);
      }
    }
    allergens
  }
}

fn allergen2value(allergen: &Allergen) -> u32 {
  match allergen {
    Allergen::Eggs => 1,
    Allergen::Peanuts => 2,
    Allergen::Shellfish => 4,
    Allergen::Strawberries => 8,
    Allergen::Tomatoes => 16,
    Allergen::Chocolate => 32,
    Allergen::Pollen => 64,
    Allergen::Cats => 128,
  }
}

fn value2allergen(value: u32) -> Option<Allergen> {
  match value {
    1 => Some(Allergen::Eggs),
    2 => Some(Allergen::Peanuts),
    4 => Some(Allergen::Shellfish),
    8 => Some(Allergen::Strawberries),
    16 => Some(Allergen::Tomatoes),
    32 => Some(Allergen::Chocolate),
    64 => Some(Allergen::Pollen),
    128 => Some(Allergen::Cats),
    _ => None,
  }
}
