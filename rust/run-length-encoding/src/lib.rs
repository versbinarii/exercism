
pub fn encode(message: &str) -> String {
    if  message.is_empty() {
        return message.to_owned();
    }
    let mut chars = message.chars().peekable();

    let mut result = String::new();

    loop {
        let mut count = 1;
        match chars.next() {
            Some(c) => {
                while chars.peek() == Some(&c) {
                    count += 1;
                    chars.next();
                }
                if count == 1 {
                    result.push(c);
                } else {
                    result.push_str(format!("{}{}", count, c).as_str());
                }         
            },
            None => break,
        }
    }
    result
}

pub fn  decode(message: &str) -> String {
    if  message.is_empty() {
        return message.to_owned();
    }

    let mut chars = message.chars().peekable();
    let mut result = String::new();

    let mut number = 1;
    loop {
        match chars.next() {
            Some(c) => {
                if c.is_digit(10) && chars.peek().unwrap().is_digit(10) {
                    let tens = c.to_digit(10).unwrap();
                    let units = chars.peek().unwrap().to_digit(10).unwrap(); 
                    number = tens * 10 + units;
                    chars.next();
                } else if c.is_digit(10) && !chars.peek().unwrap().is_digit(10) {
                    number = c.to_digit(10).unwrap();
                }
         
                if !c.is_digit(10) {              
                    for _ in  0..number {
                        result.push(c);
                    }
                    number = 1;
                }
            },
            None => break,
        }
    }
    result
}
