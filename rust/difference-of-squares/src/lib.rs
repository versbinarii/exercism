pub fn square_of_sum(i: i64) -> i64 {
    (1..i+1).fold(0, |sum, x| sum + x).pow(2)
}

pub fn sum_of_squares(i: i64) -> i64 {
    (1..i+1).fold(0,|sum, x| sum + x.pow(2))
}

pub fn difference(i: i64) -> i64 {
    square_of_sum(i) - sum_of_squares(i)
}
