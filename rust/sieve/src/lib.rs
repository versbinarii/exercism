pub fn primes_up_to(upper_bound: u64) -> Vec<u64> {
  if upper_bound <= 1 {
    return Vec::new();
  }

  let mut primes = vec![];
  let mut sieve = vec![true; (upper_bound + 1) as usize];

  for i in 2..upper_bound + 1 {
    if sieve[i as usize] {
      primes.push(i);
      for j in i.. {
        if j * i > upper_bound {
          break;
        }
        sieve[(j * i) as usize] = false;
      }
    }
  }

  primes
}
