// The code below is a stub. Just enough to satisfy the compiler.
// In order to pass the tests you can add-to or change any of this code.

#[derive(PartialEq, Debug)]
pub enum Direction {
  North,
  East,
  South,
  West,
}

pub struct Robot {
  direction: Direction,
  x: i32,
  y: i32,
}

impl Robot {
  pub fn new(x: i32, y: i32, d: Direction) -> Self {
    Self { direction: d, x, y }
  }

  pub fn turn_right(self) -> Self {
    let current_dir = {
      match self.direction {
        Direction::East => Direction::South,
        Direction::South => Direction::West,
        Direction::West => Direction::North,
        Direction::North => Direction::East,
      }
    };
    Self {
      direction: current_dir,
      x: self.x,
      y: self.y,
    }
  }

  pub fn turn_left(self) -> Self {
    let current_dir = {
      match self.direction {
        Direction::East => Direction::North,
        Direction::South => Direction::East,
        Direction::West => Direction::South,
        Direction::North => Direction::West,
      }
    };
    Self {
      direction: current_dir,
      x: self.x,
      y: self.y,
    }
  }

  pub fn advance(self) -> Self {
    match self.direction {
      Direction::East => Self {
        direction: self.direction,
        x: self.x + 1,
        y: self.y,
      },
      Direction::South => Self {
        direction: self.direction,
        x: self.x,
        y: self.y - 1,
      },
      Direction::West => Self {
        direction: self.direction,
        x: self.x - 1,
        y: self.y,
      },
      Direction::North => Self {
        direction: self.direction,
        x: self.x,
        y: self.y + 1,
      },
    }
  }

  pub fn instructions(self, instructions: &str) -> Self {
    let mut robot_state = self;
    for i in instructions.chars() {
      match i {
        'R' => robot_state = robot_state.turn_right(),
        'L' => robot_state = robot_state.turn_left(),
        'A' => robot_state = robot_state.advance(),
        _ => panic!("Unknown command"),
      }
    }
    robot_state
  }

  pub fn position(&self) -> (i32, i32) {
    (self.x, self.y)
  }

  pub fn direction(&self) -> &Direction {
    &self.direction
  }
}
