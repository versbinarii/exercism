extern crate num;

use num::Num;

#[derive(Ord, PartialOrd, Eq, PartialEq)]
pub struct Triangle<T> {
  a: T,
  b: T,
  c: T,
}

impl<T> Triangle<T>
where
  T: Num + Copy + PartialOrd,
{
  pub fn build(sides: [T; 3]) -> Option<Triangle<T>> {
    let mut s = sides.to_vec();
    s.sort_by(|a, b| a.partial_cmp(b).unwrap_or(std::cmp::Ordering::Equal));

    if s[2] <= T::zero() {
      return None;
    }
    if s[0] + s[1] < s[2] {
      return None;
    }

    Some(Self {
      a: s[0],
      b: s[1],
      c: s[2],
    })
  }

  pub fn is_equilateral(&self) -> bool {
    self.a == self.b && self.b == self.c && self.a == self.c
  }

  pub fn is_scalene(&self) -> bool {
    self.a != self.b && self.c != self.b && self.c != self.a
  }

  pub fn is_isosceles(&self) -> bool {
    self.a == self.b || self.b == self.c || self.a == self.c
  }
}
