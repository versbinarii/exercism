pub struct Luhn {
    code: String,
}

impl Luhn {
    pub fn is_valid(&self) -> bool {
        for i in self.code.chars().collect::<Vec<char>>().iter() {
            if !i.is_digit(10) && !i.is_whitespace() {
                return false;
            }
        }

        let nums: Vec<i32> = self
            .code
            .chars()
            .collect::<Vec<char>>()
            .iter()
            .filter(|&n| n.is_digit(10))
            .filter(|&n| !n.is_whitespace())
            .map(|&n| n.to_digit(10).unwrap() as i32)
            .collect::<Vec<i32>>();

        if nums.len() <= 1 {
            return false;
        }

        let repr: i32 = nums
            .iter()
            .rev()
            .enumerate()
            .map(|(i, &n)| {
                if (i + 1) % 2 == 0 && n * 2 > 9 {
                    (n * 2) - 9
                } else if (i + 1) % 2 == 0 {
                    n * 2
                } else {
                    n
                }
            })
            .collect::<Vec<i32>>()
            .iter()
            .sum::<i32>();

        repr % 10 == 0
    }
}

impl<T: ToString> From<T> for Luhn {
    fn from(input: T) -> Self {
        Luhn {
            code: input.to_string(),
        }
    }
}
