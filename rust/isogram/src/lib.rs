use std::collections::HashSet;

pub fn check(word: &str) -> bool {
    let mut check: HashSet<char> = HashSet::new();
    
    for c in word.to_lowercase().chars() {
        if !c.is_alphanumeric() {
            continue
        }
        if check.contains(&c) {
            return false
        }
        check.insert(c);
    }
    true
}
