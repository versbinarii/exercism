#[derive(Debug, PartialEq, Eq)]
pub enum Classification {
  Abundant,
  Perfect,
  Deficient,
}

pub fn classify(num: u64) -> Option<Classification> {
  if num == 0 {
    return None;
  }

  let aliquot_sum = aliquot(num);
  if aliquot_sum == num {
    Some(Classification::Perfect)
  } else if aliquot_sum > num {
    Some(Classification::Abundant)
  } else if aliquot_sum < num {
    Some(Classification::Deficient)
  } else {
    None
  }
}

fn aliquot(num: u64) -> u64 {
  let mut factors = vec![];
  for i in 1..num / 2 + 1 {
    if 0 == num % i {
      factors.push(i);
    }
  }
  factors.iter().sum()
}
