const BELOW_20: &[&'static str] = &[
  "",
  "one",
  "two",
  "three",
  "four",
  "five",
  "six",
  "seven",
  "eight",
  "nine",
  "ten",
  "eleven",
  "twelve",
  "thirteen",
  "fourteen",
  "fifteen",
  "sixteen",
  "seventeen",
  "eighteen",
  "nineteen",
];

const TENS: &[&'static str] = &[
  "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety",
];

const SCALE: &[&'static str] = &[
  "",
  "thousand",
  "million",
  "billion",
  "trillion",
  "quadrillion",
  "quintillion",
];

pub fn encode(num: u64) -> String {
  let mut number = num;

  if num == 0 {
    return String::from("zero");
  }

  if num < 20 {
    return BELOW_20[(num % 100) as usize].to_owned();
  }

  let mut text = Vec::new();

  for s in SCALE.iter() {
    let encoded_xxx = encode_xxx(number % 1000);
    if encoded_xxx == "" {
      // can be for 1000000, 1000000000 etc...
      text.insert(0, encoded_xxx);
    } else {
      text.insert(0, format!("{} {} ", encoded_xxx, s));
    }
    println!("{} {}", number / 1000, number % 1000);
    number /= 1000;
    if number == 0 {
      break;
    }
  }

  text.join("").trim().to_owned()
}

fn encode_xxx(num: u64) -> String {
  let mut result = String::new();
  let hundreds = num / 100;
  let tens_rem = num % 100;

  if hundreds != 0 {
    result.push_str(&format!("{}{}", BELOW_20[hundreds as usize], " hundred"));
    if tens_rem != 0 {
      result.push_str(" ");
    }
  }

  let tens = tens_rem / 10;
  let units = tens_rem % 10;

  if tens >= 2 {
    result.push_str(TENS[tens as usize]);
    if units != 0 {
      result.push_str(&format!("{}{}", "-", BELOW_20[units as usize]));
    }
  } else if tens_rem != 0 {
    result.push_str(BELOW_20[tens_rem as usize]);
  }

  result
}
