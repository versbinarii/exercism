use std::convert::From;

pub struct SimpleLinkedList<T> {
  head: Option<Box<Node<T>>>,
  length: usize,
}

struct Node<T> {
  data: T,
  next: Option<Box<Node<T>>>,
}

impl<T> SimpleLinkedList<T> {
  pub fn new() -> Self {
    Self {
      head: None,
      length: 0,
    }
  }

  pub fn len(&self) -> usize {
    self.length
  }

  pub fn push(&mut self, element: T) {
    let new_node = Box::new(Node {
      data: element,
      next: self.head.take(),
    });

    self.head = Some(new_node);
    self.length += 1;
  }

  pub fn pop(&mut self) -> Option<T> {
    self.head.take().map(|n| {
      let node = *n;
      self.head = node.next;
      self.length -= 1;
      node.data
    })
  }

  pub fn peek(&self) -> Option<&T> {
    self.head.as_ref().map(|n| &n.data)
  }
}

impl<T: Clone> SimpleLinkedList<T> {
  pub fn rev(&self) -> SimpleLinkedList<T> {
    let mut reversed_list = SimpleLinkedList::new();
    let mut head = self.head.as_ref();

    while let Some(node) = head {
      reversed_list.push(node.data.clone());
      head = node.next.as_ref();
    }
    reversed_list
  }
}

impl<'a, T: Clone> From<&'a [T]> for SimpleLinkedList<T> {
  fn from(item: &[T]) -> Self {
    let mut list = SimpleLinkedList::new();
    for element in item {
      list.push(element.clone());
    }
    list
  }
}

impl<T> Into<Vec<T>> for SimpleLinkedList<T> {
  fn into(mut self) -> Vec<T> {
    let mut list_as_vector = Vec::new();

    while let Some(data) = self.pop() {
      list_as_vector.push(data);
    }

    list_as_vector.reverse();
    list_as_vector
  }
}
