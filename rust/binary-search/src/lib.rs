pub fn find(array: &[i32], key: i32) -> Option<usize> {
  if array.len() == 0 {
    return None;
  }
  let mut first: isize = 0;
  let mut last: isize = array.len() as isize - 1;

  while first <= last {
    let mid = (first + last) / 2;
    if array[mid as usize] < key {
      first = mid + 1;
    } else if array[mid as usize] == key {
      return Some(mid as usize);
    } else {
      last = mid - 1;
    }
  }
  None
}
