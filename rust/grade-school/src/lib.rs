use std::collections::HashMap;

pub struct School<'a> {
  grades: HashMap<u32, Vec<&'a str>>,
}

impl<'a> School<'a> {
  pub fn new() -> School<'a> {
    School {
      grades: HashMap::new(),
    }
  }

  pub fn add(&mut self, grade: u32, student: &'a str) {
    self.grades.entry(grade).or_insert(Vec::new()).push(student);
  }

  pub fn grades(&self) -> Vec<u32> {
    let mut grades: Vec<_> = self.grades.iter().map(|(k, _)| *k).collect();
    grades.sort_by_key(|k| *k);
    grades
  }

  // If grade returned an `Option<&Vec<String>>`,
  // the internal implementation would be forced to keep a `Vec<String>` to lend out.
  // By returning an owned vector instead,
  // the internal implementation is free to use whatever it chooses.
  pub fn grade(&self, grade: u32) -> Option<Vec<String>> {
    self.grades.get(&grade).map(|v| {
      let mut cv = v.clone();
      cv.sort();
      cv.iter().map(|s| s.to_string()).collect()
    })
  }
}
