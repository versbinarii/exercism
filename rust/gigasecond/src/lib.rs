extern crate chrono;
use chrono::*;

pub fn after(sd: DateTime<UTC>) -> DateTime<UTC>{
    sd + Duration::seconds(1_000_000_000)
}
