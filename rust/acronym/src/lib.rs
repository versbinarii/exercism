const ALLOWED_INTERPUNCTION: &[char] = &[' ', '-', ':'];

pub fn abbreviate(phrase: &str) -> String {
  let mut abbreviation = String::new();
  let mut previous_char = ' ';
  if phrase.len() == 0 {
    return "".to_owned();
  }
  let _: Vec<_> = phrase
    .chars()
    .map(|c| {
      if c.is_uppercase()
        || (ALLOWED_INTERPUNCTION.iter().any(|ip| ip == &previous_char) && c.is_alphanumeric())
      {
        if !previous_char.is_uppercase() {
          // Handles consecutive capital letters [PHP]
          abbreviation.push_str(&c.to_uppercase().to_string());
        }
      }
      previous_char = c;
    })
    .collect();
  abbreviation
}
