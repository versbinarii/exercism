use std::collections::HashMap;

pub fn is_pangram(sentence: &str) -> bool {

    let mut letters = HashMap::new();
    let sentence = sentence.to_lowercase();
    let _ = sentence.chars()
        .filter(|c| c.is_alphabetic())
        .map(|c| letters.insert(c, 1)).collect::<Vec<_>>();

    letters.len() >= 26
}
