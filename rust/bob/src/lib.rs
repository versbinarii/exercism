fn is_all_caps(say: &str) -> bool {
    let mut num_of_letters = 0;
    let mut num_of_caps = 0;

    let _ = say.chars().map(|ch| {
        if ch.is_alphabetic() {
            num_of_letters += 1;
        }
        if ch.is_uppercase() {
            num_of_caps += 1;
        }
    }).collect::<Vec<()>>();

    // We need to say something.
    if num_of_letters > 0 {
        (num_of_caps == num_of_letters)
    } else {
        false
    }
}

fn is_no_content(say: &str) -> bool {
    let mut num_of_whitespace = 0;
    
    let _ = say.chars().map(|ch| {
        if ch.is_alphabetic() {
            num_of_whitespace += 1;
        }
    }).collect::<Vec<()>>();

    (num_of_whitespace == say.len())
}

pub fn reply(say: &str) -> String {

    if say.ends_with("?") {
        "Sure."
    } else if is_all_caps(say) {
        "Whoa, chill out!"
    } else if is_no_content(say) {
        "Fine. Be that way!"
    } else {
        "Whatever."
    }.to_string()
}
