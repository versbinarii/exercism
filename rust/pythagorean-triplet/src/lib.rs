/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a2 + b2 = c2

For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.
*/

pub fn find() -> Option<u32> {
    Some(triplet(1000))
}

fn triplet(sum: u32) -> u32 {
    for a in 0..sum + 1 {
        for b in a + 1..sum + 1 - a {
            let c = sum - a - b;
            if c > b && c * c == a * a + b * b {
                return a * b * c;
            }
        }
    }
    0
}
