/// Check a Luhn checksum.
pub fn is_valid(code: &str) -> bool {
    let digits: Vec<Option<u32>> = code
        .chars()
        .filter(|c| !c.is_whitespace())
        .map(|c| c.to_digit(10))
        .collect();
    if digits.len() <= 1 || digits.iter().any(|d| d.is_none()) {
        return false;
    }

    let luhn = |(i, x)| {
        if i % 2 != 0 {
            let res = 2 * x;
            if res > 9 {
                res - 9
            } else {
                res
            }
        } else {
            x
        }
    };

    let digit_sum = digits
        .iter()
        .map(|x| x.unwrap())
        .rev()
        .enumerate()
        .map(luhn)
        .sum::<u32>();

    digit_sum % 10 == 0
}
