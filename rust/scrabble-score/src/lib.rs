pub fn score(letters: &str) -> u32 {
    let mut score = 0;
    let l = letters.to_uppercase();
    for c in l.chars() {
        match c {
            'A'|'E'|'I'|'O'|
            'U'|'L'|'N'|'R'|
            'S'|'T'             => score += 1,
            'D'|'G'             => score += 2,
            'B'|'C'|'M'|'P'     => score += 3,
            'F'|'H'|'V'|'W'|'Y' => score += 4,
            'K'                 => score += 5,
            'J'|'X'             => score += 8,
            'Q'|'Z'             => score += 10,
            _                   => {}
        }
    }
    score
}
