const isPangram = sentence => {
  const letters = Array.from(sentence.toLowerCase()).filter(el => {
    return !/^[\s_"\.0-9]$/.test(el);
  });

  return [...new Set(letters)].length === 26;
};

module.exports = {
  isPangram,
};
