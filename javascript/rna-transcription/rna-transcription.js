const mapping = {
  C: 'G',
  G: 'C',
  A: 'U',
  T: 'A',
};

export function toRna(strand) {
  return strand.split('').reduce((acc, n) => {
    if (mapping[n]) {
      return acc += mapping[n];
    }
    throw new Error('Invalid input DNA.');
  }, '');
}
