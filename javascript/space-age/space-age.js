const EarthSec = 31557600;

const planets = {
  earth: 1,
  mercury: 0.2408467,
  venus: 0.61519726,
  mars: 1.8808158,
  jupiter: 11.862615,
  saturn: 29.447498,
  uranus: 84.016846,
  neptune: 164.79132,
};

function earthSec(x) {
  return x * EarthSec;
}

function calculate(time, planetTime) {
  return Math.round((time / earthSec(planetTime)) * 100) / 100;
}

function age(planet, time) {
  return calculate(time, planets[planet]);
}

module.exports = {
  age,
};
