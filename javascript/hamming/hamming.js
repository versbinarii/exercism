function compute(arr1, arr2){
  if(arr1.length !== arr2.length){
	throw new Error('left and right strands must be of equal length');
  }

  let distance = 0;
  Array.from(arr1).forEach((el, i)=>{
	if(el !== arr2[i]){
	  distance++;
	}
  });
  return distance;
}

module.exports = {
  compute
};
