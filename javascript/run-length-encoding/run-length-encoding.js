function encode(message){
  let encoded_msg = '';
  let count = 0;

  for(let i = 0; i < message.length; i++){
	if(message[i] === message[i+1]){
	  count ++;
	}else if (message[i] !== message[i+1] && count > 0){
	  /* The series ends so store it */
	  count ++;
	  encoded_msg += count + message[i];
	  count = 0;
	}else{
	  encoded_msg += message[i];
	}
  }

  return encoded_msg;
};

function decode(message){
  return message.replace(/([0-9]+)([A-Za-z ])/g, (_, length, char)=> char.repeat(length));
}

module.exports = {
  encode,
  decode
};
