
function Clock(hours, minutes){

  /* We need actual modulo operation */
  const modulo = function(num, mod){
	let modulo = num % mod;
	if(modulo < 0){
	  if(mod < 0){
		modulo = modulo - mod;
	  }else{
		modulo = modulo + mod;
	  }
	}
	return modulo;
  };
  
  const p_or_n = function(m){
	let mm = parseInt(m/60);
	if(m < 0){
	  return mm -1;
	}
	return mm;
  };

  /* Sort out negative hours */
  this.hours = modulo(hours, 24);
  
  this.hours = modulo(this.hours + p_or_n(minutes), 24);
  
  this.minutes = modulo(minutes, 60);

}

Clock.prototype.plus = function(minutes){
  return new Clock(this.hours, this.minutes + minutes);
};

Clock.prototype.minus = function(minutes){
  return new Clock(this.hours, this.minutes - minutes);  
};

Clock.prototype.equals = function(clock){
  return this.toString() === clock.toString();
};

Clock.prototype.toString = function(){
  let out_h = this.hours+'';
  let out_min = this.minutes+'';
  if(out_h.length === 1){
	out_h = '0'+this.hours;
  }

  if(out_min.length === 1){
	out_min = '0'+this.minutes;
  }

  return out_h+':'+out_min;
};

function at(hours, minutes){
  return new Clock(hours || 0, minutes || 0);
};


module.exports = {
  at
};
