const reverseString = (word) => {
  if (word.length === 0) {
    return '';
  }
  return word
    .split('')
    .reverse()
    .join('');
};

module.exports = reverseString;
