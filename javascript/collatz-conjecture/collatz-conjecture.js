function steps(number){
  let steps = 0;
  if(number < 1){
	throw new Error('Only positive numbers are allowed');
  }
  let n = number;

  while(n > 1){
	if(n %2 === 0){
	  n /= 2;
	}else{
	  n = n*3 + 1;
	}
	steps++;
  }

  return steps;
}

module.exports = {
  steps
}
