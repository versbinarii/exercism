function isFloat(number){
  return parseInt(number) !== number;
}

function convert_from(base, number){

  return number.reduce((result, digit)=>{
	if(digit >= base){
	  throw new Error('Input has wrong format');  
	}
	return result * base + digit;
  });
}

function convert_to(base, number){
  let result = [];
  if (number == 0){
	result.push(number);
  }
  while (number > 0) {
	result.push(number % base);
	number = parseInt(number / base);
  }

  return result.reverse();
}

function convert(number, base_from, base_to){

  if(!base_from || base_from < 2 || isFloat(base_from)){
	throw new Error('Wrong input base');
  }
  
  if(!base_to || base_to < 2 || isFloat(base_to)){
	throw new Error('Wrong output base');
  }

  if(number.length < 1){
	throw new Error('Input has wrong format');
  }

  if(number.length > 1 && number[0] === 0){
	throw new Error('Input has wrong format');
  }

  if(number.some((d)=> { return d < 0; })){
	throw new Error('Input has wrong format');	
  }

  let converted = convert_from(base_from, number);
  return convert_to(base_to, converted);
}

module.exports = {
  convert
};
