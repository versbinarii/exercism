

module Acronym (abbreviate) where

import Data.Char (isUpper, toUpper, isAlpha)

abbreviate :: String -> String
abbreviate "" = ""
abbreviate (p:d:ps) | isUpper p && isUpper d = abbreviate (p:ps)
                    | not (isAlpha p) && isAlpha d = toUpper d : abbreviate ps 
abbreviate (x:xs) | isUpper x = x:abbreviate xs
                  | otherwise = abbreviate xs
