module SpaceAge (Planet(..), ageOn) where

data Planet = Mercury
            | Venus
            | Earth
            | Mars
            | Jupiter
            | Saturn
            | Uranus
            | Neptune

earthSec:: Float -> Float
earthSec x = x * 31557600

getYear :: Planet -> Float
getYear Mercury = earthSec 0.2408467
getYear Earth = earthSec 1
getYear Venus = earthSec 0.61519726
getYear Mars = earthSec 1.8808158
getYear Jupiter = earthSec 11.862615
getYear Saturn = earthSec 29.447498
getYear Uranus = earthSec 84.016846
getYear Neptune = earthSec 164.79132

ageOn :: Planet -> Float -> Float
ageOn planet sec = sec / getYear planet 
